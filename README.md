# Project Name

Al Mundo Challenge

## Installation

La solución se hizo siguiendo el modelo mean, tanto sea los web service y la app se encuentran en el mismo proyecto. <br />
Puede verse un demo funcional en: https://almundo-demo.herokuapp.com/ <br />
<br />
Aclaraciones<br />
===============<br />
Por cuestiones de tiempo algunas cosas no fueron incluidas:<br />
Al hacer click en "todas las estrellas" no se desseleccionan las individuales y viceversa<br />
Faltó incluir el paginado. No es complicado, habría que a los web services agregarles un parámetro "número de página" y hacer la botonera inferior. En este demo el webserver solo haría in slice.<br />
<br />
<br />

## Usage

node server.js

## Credits

Andrés Aiello

## License

TODO: Write license
