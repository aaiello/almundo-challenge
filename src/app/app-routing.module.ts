import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {HotelListComponent} from './hotels/hotel-list/hotel-list.component';
import {HotelDetailsComponent} from './hotels/hotel-details/hotel-details.component';
import {HotelMainPageComponent} from './hotels/hotel-main-page/hotel-main-page.component';

const appRoutes: Routes = [
    { path: '', component: HotelMainPageComponent },
    { path: 'hotel/:id', component: HotelDetailsComponent },
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            {
                enableTracing: true, // <-- debugging purposes only

            }
        )
    ],
    exports: [
        RouterModule
    ],
})
export class AppRoutingModule { }


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/