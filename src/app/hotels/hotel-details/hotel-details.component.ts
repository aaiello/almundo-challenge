import { Component, OnInit, Input } from '@angular/core';
import { Hotel } from '../hotel';
import { HotelService } from '../hotel.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'hotel-details',
  templateUrl: './hotel-details.component.html',
  styleUrls: ['./hotel-details.component.css'],
    providers: [HotelService]
})

export class HotelDetailsComponent {
  hotel$: Observable<Hotel>;

  constructor (private hotelService: HotelService,
               private route: ActivatedRoute,
               private router: Router
  ) {}

    ngOnInit() {
        this.hotel$ = this.route.paramMap.pipe(
            switchMap((params: ParamMap) =>
                this.hotelService.getHotel(parseInt(params.get('id'))))
        );
    }
}