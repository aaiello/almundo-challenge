export class Hotel {
  constructor(
      public id: string,
      public name: string,
      public stars: number,
      public price: string,
      public image: string,
      public amenities: string[]) {
  }

}
