import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import {Filter} from './filter';

@Component({
  selector: 'filter-form',
  templateUrl: './filter-form.component.html',
  styleUrls: ['./filter-form.component.css']
})

export class FilterFormComponent implements OnInit {
    @Output() onFilterFormSubmit: EventEmitter<Filter>;
    filterForm: FormGroup;

    constructor(private fb: FormBuilder) { // <--- inject FormBuilder
        this.createForm();
        this.onFilterFormSubmit = new EventEmitter();
    }

    ngOnInit() {}

    createForm() {
        this.filterForm = this.fb.group({
            name: '',
            allstars: '',
            star1: '',
            star2: '',
            star3: '',
            star4: '',
            star5: ''
        });
    }



    onFormSubmit(): void {
        //const name = this.filterForm.get('name').value;
        const filter = new Filter();
        filter.name = this.filterForm.get('name').value;

        if(this.filterForm.get('allstars').value){
            filter.stars = [1, 2, 3, 4, 5];
        }else{
            filter.stars = [];
            if(this.filterForm.get('star1').value){
                filter.stars.push(1);
            }
            if(this.filterForm.get('star2').value){
                filter.stars.push(2);
            }
            if(this.filterForm.get('star3').value){
                filter.stars.push(3);
            }
            if(this.filterForm.get('star4').value){
                filter.stars.push(4);
            }
            if(this.filterForm.get('star5').value){
                filter.stars.push(5);
            }
            if(filter.stars.length == 0){
                filter.stars = [1, 2, 3, 4, 5];
            }
        }

        this.onFilterFormSubmit.emit(filter);
    }
}
