import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import {Filter} from './filter-form/filter';
import 'rxjs/add/operator/toPromise';

import { Hotel } from './hotel';



@Injectable()
export class HotelService {
    private hotelsUrl = '/api/hotels';

    constructor (private http: Http) {}

    // get("/api/hotels")
    getHotels(): Promise<Hotel[]> {
      return this.http.get(this.hotelsUrl)
                 .toPromise()
                 .then(response => response.json() as Hotel[])
                 .catch(this.handleError);
    }

    // get("/api/hotels/:filter")
    getHotelsFilter(filter: Filter): Promise<Hotel[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.hotelsUrl, filter, options)
            .toPromise()
            .then(response => response.json() as Hotel[])
            .catch(this.handleError);
    }

    // get("/api/hotels/:id")
    getHotel(id: number): Promise<Hotel> {
        const url = this.hotelsUrl + '/' + id.toString();
        return this.http.get(url)
            .toPromise()
            .then(response => response.json() as Hotel)
            .catch(this.handleError);
    }


    private handleError (error: any): Promise<any> {
      let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
      console.error(errMsg); // log to console
      return Promise.reject(errMsg);
    }
}