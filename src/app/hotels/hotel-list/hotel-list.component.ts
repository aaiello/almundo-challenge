import {Component, EventEmitter, OnInit, Output, Input} from '@angular/core';
import { Hotel } from '../hotel';

// Importamos los componentes para trabajar con las rutas
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.css']
})

export class HotelListComponent implements OnInit {
    @Input() hotels: Hotel[];

    constructor(private route: ActivatedRoute,
                private router: Router) { }
    ngOnInit() { }
}
