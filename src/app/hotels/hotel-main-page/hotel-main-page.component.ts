import {Component, EventEmitter, OnInit, Output, Input} from '@angular/core';
import {Hotel} from '../hotel';
import {HotelService} from '../hotel.service';
import {Filter} from '../filter-form/filter';


// Importamos los componentes para trabajar con las rutas
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'hotel-main-page',
  templateUrl: './hotel-main-page.component.html',
  styleUrls: ['./hotel-main-page.component.css'],
    providers: [HotelService]
})

export class HotelMainPageComponent implements OnInit {
    hotels: Hotel[];

    constructor(private hotelService: HotelService) { }

    ngOnInit() {
        this.hotelService
            .getHotels()
            .then((hotels: Hotel[]) => {
                this.hotels = hotels.map((hotel) => {
                    return hotel;
                });
            });

    }

    onFilterFormSubmit(filter: Filter): void {
        this.hotelService
            .getHotelsFilter(filter)
            .then((hotels: Hotel[]) => {
                this.hotels = hotels.map((hotel) => {
                    return hotel;
                });
            });


    }

}
