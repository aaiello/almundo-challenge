import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { HotelDetailsComponent } from './hotels/hotel-details/hotel-details.component';
import { HotelListComponent } from './hotels/hotel-list/hotel-list.component';
import { FilterFormComponent } from './hotels/filter-form/filter-form.component';
import { HotelMainPageComponent } from './hotels/hotel-main-page/hotel-main-page.component';

import { ReactiveFormsModule } from '@angular/forms';

import { Router } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  declarations: [
      AppComponent,
      HotelDetailsComponent,
      HotelListComponent,
      FilterFormComponent,
      HotelMainPageComponent,
  ],  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
    constructor(router: Router) {
        console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
    }
}
