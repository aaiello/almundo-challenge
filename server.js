var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.json());

// Create link to Angular build directory
var distDir = __dirname + "/dist/";
app.use(express.static(distDir));

// Initialize the app.
var server = app.listen(process.env.PORT || 8080, function () {
  var port = server.address().port;
  console.log("App now running on port", port);
});

// CONTACTS API ROUTES BELOW

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}
/*  "/api/hotels"
 *    GET: finds all hotels
 *    POST: filter hotels list
 */

app.get("/api/hotels", function(req, res) {
    var fs = require('fs');
    var unJson = fs.readFileSync('./data/data.json', 'utf8');
    var docs = JSON.parse(unJson);

    res.status(200).json(docs);
});

app.post("/api/hotels", function(req, res) {
    var filter = req.body;
    console.log("Filtro:");
    console.log(filter);
    var fs = require('fs');
    var unJson = fs.readFileSync('./data/data.json', 'utf8');
    var docs = JSON.parse(unJson);

    docs =  docs
        .filter(function (item) {
        return item.name.toLowerCase().indexOf(filter.name.toLowerCase())>-1;
        })
        .filter(function (item) {
            return filter.stars.indexOf(item.stars)> -1 ;
        })
    res.status(200).json(docs);
});

/*  "/api/hotels/:id"
 *    GET: finds one hotel
 */

app.get("/api/hotels/:id", function(req, res) {
    var fs = require('fs');
    var unJson = fs.readFileSync('./data/data.json', 'utf8');
    var docs = JSON.parse(unJson);

    docs =  docs
        .filter(function (item) {
            return item.id===req.params.id;;
        })

    res.status(200).json(docs[0]);
});


app.all("/*", function(req, res, next) {
    res.sendFile('index.html', {root: distDir });
});
